+ 画面確認

http://192.168.59.59/sites
にアクセス！

+ Vagrant 関連

> 仮想マシンの起動
$ vagrant up

> 仮想マシンの電源off
$ vagrant halt

> 仮想マシンに接続する（ターミナルでログイン）
$ vagrant ssh

> （仮想マシンのターミナルから）ログアウト
$ exit

+ Git 関連
（※ファイル名はシングルクォート不要）

>現ドキュメントの現状確認
$ git status

>差分を(リモートリポジトリから)持ってきて、差分を取り込む
$ git pull origin master

>前回との差分の確認
$ git diff file1 file2

>gitでの管理下へ指定ディレクトリ内(現在のディレクトリ)の内容を登録する
$ git add site/README.txt site/sample.php

(例) 変更したファイルをすべてステージングに追加する
$ git add -A

>コメント(ほげほげ)をつけて記録をつける
$ git commit -m “ほげほげ”

>集中管理(リモートリポジトリ)に送る
$ git push origin master

>変更点を確認する
$ git log

(例) 変更したファイルの詳細を確認する
$ git log -p

